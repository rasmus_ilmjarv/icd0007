<?php

require_once "functions.php";
require_once "storage/old-DAO.php";

$EDIT_MESSAGE = "editSuccess";
$SAVE_MESSAGE = "addSuccess";
$DELETE_MESSAGE = "delSuccess";

print_r($_POST);
$fname_length = strlen($_POST["firstName"]);
$lname_length = strlen($_POST["lastName"]);

$data = "firstName=".$_POST["firstName"]."&"."lastName=".$_POST["lastName"]."&"."grade=".$_POST["grade"];

if (key_exists("id", $_POST)) $data = $data . "&id=". $_POST["id"];

if (1 > $fname_length || $fname_length > 21) {
    header("Location: author-add.php?action=" . "fname&" . $data);

} elseif (2 > $lname_length || $lname_length > 22) {
    header("Location: author-add.php?action=" . "lname&" . $data);

} elseif (!key_exists("grade", $_POST)) {
    header('Location: author-add.php?action=' . "authorGrade" . "&" . $data);

} elseif (key_exists("deleteButton", $_POST)) {
    deleteAuthor($_POST["id"]);
    header("Location: author-list.php?msg=".urlencode($DELETE_MESSAGE));
} elseif (key_exists("id", $_POST)) {
    deleteAuthor($_POST["id"]);

    $data = urlencode($_POST["firstName"]).";".urlencode($_POST["lastName"]).";".urlencode($_POST["grade"]);

    file_put_contents("storage/author_storage.txt", $data.PHP_EOL, FILE_APPEND);

    header("Location: author-list.php?msg=".$EDIT_MESSAGE);

} else {
    $data = urlencode($_POST["firstName"]).";".urlencode($_POST["lastName"]).";".urlencode($_POST["grade"]);

    file_put_contents("storage/author_storage.txt", $data.PHP_EOL, FILE_APPEND);

    header("Location: author-list.php?msg=".$SAVE_MESSAGE);

}
