<?php

require_once "functions.php";
require_once "storage/old-DAO.php";

function validate_author(array $post) {
    $EDIT_MESSAGE = "editSuccess";
    $SAVE_MESSAGE = "addSuccess";
    $DELETE_MESSAGE = "delSuccess";

    print_r($post);
    $fname_length = strlen($post["firstName"]);
    $lname_length = strlen($post["lastName"]);

    $data = "firstName=".$post["firstName"]."&"."lastName=".$post["lastName"]."&"."grade=".$post["grade"];

    if (key_exists("id", $post)) $data = $data . "&id=". $post["id"];

    if (key_exists("deleteButton", $post)) {
        delete_author_DB(intval($post["id"]));
        header("Location: index.php?cmd=author-list&msg=".urlencode($DELETE_MESSAGE));

    } elseif (1 > $fname_length || $fname_length > 21) {
        header("Location:  index.php?cmd=author-form&action=" . "fname&" . $data);

    } elseif (2 > $lname_length || $lname_length > 22) {
        header("Location: index.php?cmd=author-form&action=" . "lname&" . $data);

    }  elseif (!key_exists("grade", $post)) {
        header('Location: index.php?cmd=author-form&action=' . "authorGrade" . "&" . $data);

    } elseif (key_exists("id", $post)) {
        $grade = is_numeric($post['grade']) ? intval($post['grade']) : 0;
        print_r($_POST);

        update_author_DB($post['id'], $post['firstName'], $post['lastName'], $grade);

        header("Location: index.php?cmd=author-list&msg=".$EDIT_MESSAGE);

    } else {
        save_author_DB($post['firstName'], $post['lastName'], is_numeric($post['grade']) ? intval($post['grade']) : 0);

        header("Location: index.php?cmd=author-list&msg=".$SAVE_MESSAGE);

    }
}
