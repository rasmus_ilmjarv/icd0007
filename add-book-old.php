<?php

require_once "functions.php";
require_once "storage/old-DAO.php";

$EDIT_MESSAGE = "editSuccess";
$SAVE_MESSAGE = "addSuccess";
$DELETE_MESSAGE = "delSuccess";

print_r($_POST);

$authors = "jfrdlöäyvyFö486-dffjgsissdlca";

$length = strlen($_POST["title"]);

$data = "title=".urlencode($_POST["title"])."&"."grade=".urlencode($_POST["grade"])."&"."isRead=".urlencode($_POST["isRead"]);

if (key_exists("id", $_POST)) $data = $data . "&id=". $_POST["id"];

if (3 > $length || $length > 23) {
    header('Location: book-add.php?action=' . "title" . "&" . $data);

} elseif (!key_exists("grade", $_POST)) {
    header('Location: book-add.php?action=' . "grade" . "&" . $data);

} elseif (key_exists("deleteButton", $_POST)) {
    deleteBook($_POST["id"]);
    header("Location: index.php?msg=".urlencode($DELETE_MESSAGE));

} elseif (key_exists("id", $_POST)) {
    deleteBook($_POST["id"]);

    $data = urlencode($_POST["title"]).";".$authors.";".urlencode($_POST["grade"]).";".urlencode($_POST["isRead"]);

    file_put_contents("storage/book_storage.txt", $data.PHP_EOL, FILE_APPEND);

    header("Location: index.php?msg=".urlencode($EDIT_MESSAGE));

} else {
    $data = urlencode($_POST["title"]).";".$authors.";".urlencode($_POST["grade"]).";".urlencode($_POST["isRead"]);

    file_put_contents("storage/book_storage.txt", $data.PHP_EOL, FILE_APPEND);

    header("Location: index.php?msg=".urlencode($SAVE_MESSAGE));
}


