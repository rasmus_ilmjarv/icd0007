<?php

require_once "functions.php";
require_once "storage/old-DAO.php";

$EDIT_MESSAGE = "editSuccess";
$SAVE_MESSAGE = "addSuccess";
$DELETE_MESSAGE = "delSuccess";

print_r($_POST);

$authortmp = [];
$authortmp[] = 1;

$length = strlen($_POST["title"]);

$is_read = key_exists("isRead", $_POST);

var_dump($is_read);

$data = "title=".urlencode($_POST["title"])."&"."grade=".urlencode($_POST["grade"])."&"."isRead=".urlencode($_POST["isRead"])."&"."author1=".urlencode($_POST["author1"])."&"."author2=".urlencode($_POST["author2"]);

if (key_exists("id", $_POST)) $data = $data . "&id=". $_POST["id"];

if (!(is_int(intval($_POST["author1"])) && is_int(intval($_POST["author2"])))) {
    save_book_DB(htmlspecialchars($_POST['title'], ENT_QUOTES), $authortmp, htmlspecialchars(intval($_POST['grade']), ENT_QUOTES), $is_read);
    header('Location: index.php');
}

if (3 > $length || $length > 23) {
    header('Location: book-add.php?action=' . "title" . "&" . $data);

} elseif ($_POST["author1"] == 0 && $_POST["author2"] == 0) {
    header('Location: book-add.php?action=' . "author_missing" . "&" . $data);
}elseif ($_POST["author1"] == $_POST["author2"]) {
    header('Location: book-add.php?action=' . "author_overlap" . "&" . $data);
} elseif (!key_exists("grade", $_POST)) {
    header('Location: book-add.php?action=' . "grade" . "&" . $data);

} elseif (key_exists("deleteButton", $_POST)) {
    delete_book_DB($_POST["id"]);
    header("Location: index.php?msg=".urlencode($DELETE_MESSAGE));

} elseif (key_exists("id", $_POST)) {
    $authors = [];
    if ($_POST["author1"] != 0) $authors[] = $_POST["author1"];
    if ($_POST["author2"] != 0) $authors[] = $_POST["author2"];

    update_book_DB($_POST['id'], htmlspecialchars($_POST['title'], ENT_QUOTES), $authors, intval($_POST['grade']), $is_read);

    header("Location: index.php?msg=".urlencode($EDIT_MESSAGE));

} else {
    $authors = [];
    if ($_POST["author1"] != 0) $authors[] = intval($_POST["author1"]);
    if ($_POST["author2"] != 0) $authors[] = intval($_POST["author2"]);

    save_book_DB(htmlspecialchars($_POST['title'], ENT_QUOTES), $authors, intval($_POST['grade']), $is_read);

    header("Location: index.php?msg=".urlencode($SAVE_MESSAGE));
}

