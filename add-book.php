<?php

require_once "functions.php";
require_once "storage/old-DAO.php";

function validate_book(array $post) {
    $EDIT_MESSAGE = "editSuccess";
    $SAVE_MESSAGE = "addSuccess";
    $DELETE_MESSAGE = "delSuccess";

    print_r($post);

    $authortmp = [];
    $authortmp[] = 1;

    $length = strlen($post["title"]);

    $is_read = key_exists("isRead", $post);

    var_dump($is_read);

    $data = "title=".urlencode($post["title"])."&"."grade=".urlencode($post["grade"])."&"."isRead=".urlencode($post["isRead"])."&"."author1=".urlencode($post["author1"])."&"."author2=".urlencode($post["author2"]);

    if (key_exists("id", $post)) $data = $data . "&id=". $post["id"];

    if (!(is_int(intval($post["author1"])) && is_int(intval($post["author2"])))) {
        save_book_DB(htmlspecialchars($post['title'], ENT_QUOTES), 1, 3, $is_read);
        header('Location: index.php');
    }

    if (key_exists("deleteButton", $post)) {
        delete_book_DB($post["id"]);
        header("Location: index.php?cmd=book-list&msg=".urlencode($DELETE_MESSAGE));

    } elseif (3 > $length || $length > 23) {
        header('Location: index.php?cmd=book-form&action=' . "title" . "&" . $data);

    } elseif (key_exists("id", $post)) {
        $authors = [];
        if ($post["author1"] != 0) $authors[] = $post["author1"];
        if ($post["author2"] != 0) $authors[] = $post["author2"];

        update_book_DB($post['id'], htmlspecialchars($post['title'], ENT_QUOTES), $authors, is_numeric($post['grade']) ? intval($post['grade']) : 0, $is_read);

        header("Location: index.php?cmd=book-list&msg=".urlencode($EDIT_MESSAGE));

    } else {
        $authors = [];
        if ($post["author1"] != 0) $authors[] = intval($post["author1"]);
        if ($post["author2"] != 0) $authors[] = intval($post["author2"]);

        save_book_DB(htmlspecialchars($post['title'], ENT_QUOTES), $authors, is_numeric($post['grade']) ? intval($post['grade']) : 0, $is_read);

        header("Location: index.php?cmd=book-list&msg=".urlencode($SAVE_MESSAGE));
    }
}


