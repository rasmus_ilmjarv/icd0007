<?php
require_once "functions.php";
require_once "storage/old-DAO.php";

$doError = false;
$errorText = "";
$change = false;
$has_id = false;

$first_name = "";
$last_name = "";
$grade_value = 3;

if (key_exists("action", $_GET)) {
    $type = $_GET["action"];
    if ($type == "edit") {
        $change = true;
        $id = intval($_GET["id"]);
        $to_fill = read_author_DB($id);
        $first_name = $to_fill["firstName"];
        $grade_value = intval($to_fill["grade"]);
        $last_name = $to_fill["lastName"];
    } else {
        if (key_exists("id", $_GET)) {
            $has_id = true;
            $id = intval($_GET["id"]);
        }
        $doError = true;
        $errorText = createErrorText($type);
    }
}
if (key_exists("firstName", $_GET)) {
    $first_name = $_GET["firstName"];
    $last_name = $_GET["lastName"];
    $grade_value = intval($_GET["grade"] ?? "");
}
?>

<!DOCTYPE html>
<html lang="et">
<head>
    <meta charset="UTF-8">
    <title>Lisa autor</title>
    <link rel="stylesheet" type="text/css" href="style.css" >
</head>
<body id="author-form-page">
    <table class="edge_creator">
        <tr>
            <td></td>
            <td class="main_content">
                <div class="page">
                    <nav>
                        <div>
                            <a href="index.php" id="book-list-link">Raamatud</a>
                            <span>|</span>
                            <a href="book-add.php" id="book-form-link">Lisa raamat</a>
                            <span>|</span>
                            <a href="author-list.php" id="author-list-link">Autorid</a>
                            <span>|</span>
                            <a href="author-add.php" id="author-form-link">Lisa autor</a>
                        </div>
                    </nav>
                    <div id="main">
                        <?php if ($doError): ?>
                            <div id="error-block"><?php echo $errorText?></div>
                        <?php endif; ?>
                        <div class="form_container">
                            <form id="input_form" action="add-author.php" method="post">
                                <div class="form_row">
                                    <div class="label_container form_member">
                                        <label for="fname">
                                            Eesnimi:
                                        </label>
                                    </div>
                                    <div class="input_container form_member">
                                        <input type="text" id="fname" name="firstName" value=<?php echo '"'.$first_name.'"'?>>
                                    </div>
                                </div>
                                <div class="form_row">
                                    <div class="label_container form_member">
                                        <label for="lname">
                                            Perenimi:
                                        </label>
                                    </div>
                                    <div class="input_container form_member">
                                        <input type="text" id="lname" name="lastName" value=<?php echo '"'.$last_name.'"'?>>
                                    </div>
                                </div>
                                <div class="form_row">
                                    <div class="label_container form_member">
                                        <label>
                                            Hinne:
                                        </label>
                                    </div>
                                    <div class="input_container form_member">
                                        <?php foreach (range(1, 5) as $grade): ?>

                                            <input type="radio" name="grade" <?= $grade == $grade_value ? 'checked' : ''; ?> value="<?= $grade ?>" id=<?= "grade".$grade ?>/>
                                            <label for=<?= "grade".$grade ?>><?= $grade ?></label>

                                        <?php endforeach; ?>
                                    </div>
                                    <?php if ($change || $has_id): ?>
                                        <input type="hidden" value=<?php echo '"'.$id.'"' ?> id="id" name="id">
                                    <?php endif; ?>
                                </div>
                                <div class="form_row">
                                    <div class="button_container">
                                        <input type="submit" value="Salvesta" name="submitButton" id="submitButton">
                                        <?php if ($change): ?>
                                            <input type="submit" value="Kustuta" id="deleteButton" name="deleteButton">
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <footer>Sample text</footer>
                </div>
            </td>
            <td></td>
        </tr>
    </table>
</body>
</html>