<?php

require_once "functions.php";
require_once "storage/old-DAO.php";

$write_message = key_exists("msg", $_GET);
$message = "";
if ($write_message) $message = createMSGText($_GET["msg"], "authors");

$counter = 0;
?>


<!DOCTYPE html>
<html lang="et">
<head>
    <meta charset="UTF-8">
    <title>Autorid</title>
    <link rel="stylesheet" type="text/css" href="style.css" >
</head>
<body id="author-list-page">
    <table class="edge_creator">
        <tr>
            <td></td>
            <td class="main_content">
                <div class="page">
                    <nav>
                        <div>
                            <a href="index.php" id="book-list-link">Raamatud</a>
                            <span>|</span>
                            <a href="book-add.php" id="book-form-link">Lisa raamat</a>
                            <span>|</span>
                            <a href="author-list.php" id="author-list-link">Autorid</a>
                            <span>|</span>
                            <a href="author-add.php" id="author-form-link">Lisa autor</a>
                        </div>
                    </nav>
                    <?php if($write_message): ?>
                        <?php error_log($message)?>
                        <div id="message-block"><?php echo $message?></div>
                    <?php endif;?>
                    <div id="main">
                        <div class="body new-book-list table_head">
                            <div class="new-book-list first_name">Eesnimi</div>
                            <div class="new-book-list last_name">Perekonnanimi</div>
                            <div class="new-book-list score_title">Hinne</div>
                            <div class="divider"></div>
                        </div>
                        <?php foreach (read_authors_DB() as $entry): ?>
                            <div class="body new-book-list table_entry">
                                <div class="new-book-list first_name">
                                    <?php if (key_exists('id', $entry)): ?>
                                        <a href=<?php echo "author-add.php?action=edit&id=". $entry['id']?>>
                                            <?php echo $entry["firstName"]?>
                                        </a>
                                    <?php else: ?>
                                        <a href=<?php echo "author-add.php?action=edit&id=". $counter++?>>
                                            <?php echo $entry["firstName"]?>
                                        </a>
                                    <?php endif;?>
                                </div>
                                <div class="new-book-list last_name">
                                    <?php echo $entry["lastName"]?>
                                </div>
                                <div class="new-book-list score">
                                    <?php foreach (range(1, $entry["grade"]) as $value): ?>
                                        <span class="score_given">★</span>
                                    <?php endforeach; ?>
                                    <?php if ($entry["grade"] < 5) foreach (range($entry["grade"], 4) as $value): ?>
                                        <span class="no_stars">★</span>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <footer>Sample text</footer>
                </div>
            </td>
            <td></td>
        </tr>
    </table>
</body>
</html>