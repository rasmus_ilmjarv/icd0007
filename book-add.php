<?php
    require_once "functions.php";
    require_once "storage/old-DAO.php";

    $doError = false;
    $errorText = "";
    $change = false;
    $has_id = false;

    $title = "";
    $grade_value = 3;
    $read = false;
    $author_selected = [];
    $author_selected[] = 0;
    $author_selected[] = 0;

    if (key_exists("action", $_GET)) {
        $type = $_GET["action"];
        if ($type == "edit") {
            $change = true;
            $id = intval($_GET["id"]);
            $to_fill = read_book_DB($id);
            $title = $to_fill["title"];
            $author_selected = $to_fill["author"];
            if (count($author_selected) == 1) $author_selected[] = 0;
            $grade_value = intval($to_fill["grade"]);
            $read = $to_fill["isRead"];
        } else {
            if (key_exists("id", $_GET)) {
                $has_id = true;
                $id = intval($_GET["id"]);
            }
            $doError = true;
            $errorText = createErrorText($type);
        }
    }
    if (key_exists("title", $_GET)) {
        $title = $_GET["title"];
        $grade_value = intval($_GET["grade"] ?? "");
        $author_selected = [];
        $author_selected[] = intval($_GET["author1"]);
        $author_selected[] = intval($_GET["author2"]);
        $read = $_GET["isRead"] == "on";
    }

    $authors = read_authors_DB();

?>


<!DOCTYPE html>
<html lang="et">
<head>
    <meta charset="UTF-8">
    <title>Lisa raamat</title>
    <link rel="stylesheet" type="text/css" href="style.css" >
</head>
<body id="book-form-page">
<table class="edge_creator">
    <tr>
        <td></td>
        <td class="main_content">
            <div class="page">
                <nav>
                    <div>
                        <a href="index.php" id="book-list-link">Raamatud</a>
                        <span>|</span>
                        <a href="book-add.php" id="book-form-link">Lisa raamat</a>
                        <span>|</span>
                        <a href="author-list.php" id="author-list-link">Autorid</a>
                        <span>|</span>
                        <a href="author-add.php" id="author-form-link">Lisa autor</a>
                    </div>
                </nav>
                <div id="main">
                    <?php if ($doError): ?>
                        <div id="error-block"><?php echo $errorText?></div>
                    <?php endif; ?>
                    <div class="form_container">
                        <form id="input_form" method="post" action="add-book.php">
                            <div class="form_row">
                                <div class="label_container form_member">
                                    <label for="title">
                                        Pealkiri:
                                    </label>
                                </div>
                                <div class="input_container form_member">
                                    <input type="text" id="title" name="title" value=<?php echo '"'.$title.'"'?>>
                                </div>
                            </div>
                            <div class="form_row">
                                <div class="label_container form_member">
                                    <label for="author1">
                                        Autor:
                                    </label>
                                </div>
                                <div class="input_container form_member">
                                    <select id="author1" class="author_select" name="author1">
                                        <option value="0" <?= $author_selected[0] == 0 ? 'selected="selected"' : 'a'; ?>></option>
                                        <?php foreach ($authors as $author): ?>
                                            <option value="<?= $author['id']?>" id="author1" <?= $author_selected[0] == $author['id'] ? 'selected="selected"' : ''; ?>><?= $author["firstName"] . " " . $author["lastName"]?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form_row">
                                <div class="label_container form_member">
                                    <label for="author2">
                                        Autor 2:
                                    </label>
                                </div>
                                <div class="input_container form_member">
                                    <select id="author2" class="author_select" name="author2">
                                        <option value="0" <?= $author_selected[1] == 0 ? 'selected="selected"' : 'a'; ?>></option>
                                        <?php foreach ($authors as $author): ?>
                                            <option value="<?= $author['id']?>" id="author2" <?= $author_selected[1] == $author['id'] ? 'selected="selected"' : ''; ?>><?= $author["firstName"] . " " . $author["lastName"]?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form_row">
                                <div class="label_container form_member">
                                    <label>
                                        Hinne:
                                    </label>
                                </div>
                                <div class="input_container form_member">
                                    <?php foreach (range(1, 5) as $grade): ?>

                                        <input type="radio" name="grade" <?= $grade == $grade_value ? 'checked' : ''; ?> value="<?= $grade ?>" />
                                        <label><?= $grade ?></label>

                                    <?php endforeach; ?>
                                </div>
                            </div>
                            <div class="form_row">
                                <div class="label_container form_member">
                                    <label for="isRead">
                                        Loetud:
                                    </label>
                                </div>
                                <div class="input_container form_member">
                                    <input type="checkbox" id="isRead" name="isRead" <?= $read == true ? 'checked' : ''; ?>>
                                </div>
                            </div>
                            <?php if ($change || $has_id): ?>
                                <input type="hidden" value=<?php echo '"'.$id.'"' ?> id="id" name="id">
                            <?php endif; ?>
                            <div class="form_row">
                                <div class="button_container">
                                    <input type="submit" value="Salvesta" id="submitButton" name="submitButton">
                                    <?php if ($change): ?>
                                        <input type="submit" value="Kustuta" id="deleteButton" name="deleteButton">
                                    <?php endif; ?>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <footer>Sample text</footer>
            </div>
        </td>
        <td></td>
    </tr>
</table>
</body>
</html>