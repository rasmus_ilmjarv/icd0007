<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Fahrenheit to Celsius</title>
</head>
<body>

    <nav>
        <a href="index.html">Celsius to Fahrenheit</a> |
        <a href="f2c.html">Fahrenheit to Celsius</a>
    </nav>

    <main>

        <h3>Fahrenheit to Celsius</h3>

        <?php
        if (!$_POST["temperature"] && $_POST["temperature"] != "0") {
            print "Insert temperature";
        } elseif (!(intval($_POST["temperature"])) && $_POST["temperature"] != "0") {
            print "Temperature must be an integer";
        } else {
            $temp = intval($_POST["temperature"]);

            print $temp;
            print " decrees in Fahrenheit is ";
            print ($temp - 32) / (9/5);
            print " decrees in Celsius";
        }

        ?>

    </main>

</body>
</html>
