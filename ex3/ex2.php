<?php

$numbers = [1, 2, '3', 6, 2, 3, 2, 3];

function isInList($list, $elementToBeFound) {
    foreach ($list as $element) {
        if ($element == $elementToBeFound) {
            return true;
        }
    }
    return false;
}

print nl2br("\n");
print var_dump(isInList([1, 2, 3], 2));
print nl2br("\n");
print var_dump(isInList([1, 2, 3], 4));
