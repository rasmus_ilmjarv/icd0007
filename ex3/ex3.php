<?php

$numbers = [1, 2, 5, 6, 2, 11, 2, 7];


function getOddNumbers($list) {
    $odds = [];
    $ptr = 0;

    foreach ($list as $num) {
        if ($num % 2 == 1) {
            $odds[$ptr] = $num;
            $ptr++;
        }
    }

    return $odds;
}

print nl2br("\n");
print_r(getOddNumbers($numbers));

