<?php
for ($i = 1; $i <= 15; $i++) {
    $toPrint = "";

    if ($i % 3 == 0) {
        $toPrint = $toPrint."Fizz";
    } elseif ($i % 5 == 0) {
        $toPrint = $toPrint."Buzz";
    } else {
        $toPrint = $i;
    }

    print nl2br($toPrint."\n");
}
