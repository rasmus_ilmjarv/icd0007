<?php

include_once __DIR__ . '/Post.php';

const DATA_FILE = __DIR__ . '/data/posts.txt';

/*printPosts(getAllPosts());

savePost(new Post('Html', "some text about html"));*/

function getAllPosts() : array {

    $lines = file(DATA_FILE);
    $posts = [];

    foreach ($lines as $line) {
        $info = explode(";", $line);
        $post = new Post(urldecode($info[0]), urldecode(trim($info[1])));
        $posts[] = $post;
    }

    return $posts;
}

function savePost(Post $post) : void {

    $info = join(";", [urlencode($post->title), urlencode($post->text)])."\n";

    file_put_contents(DATA_FILE, $info, FILE_APPEND);

}

function printPosts(array $posts) {
    foreach ($posts as $post) {
        print $post . PHP_EOL;
    }
}