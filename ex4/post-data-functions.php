<?php

include_once __DIR__ . '/Post.php';

const DATA_FILE = __DIR__ . '/data/posts.txt';

$post = new Post('Title 1', 'Text 1');

savePost($post);

function savePost(Post $post) : string
{
    $id = $post->id;

    if ($id != '') {
        deletePostById($id);
        $line = urlencode($post->id) . ';' . urlencode($post->title) . ';' . urlencode($post->text) . PHP_EOL;
    } else {
        $posts = getAllPosts();
        do {
            $n = 7;
            $result = bin2hex(random_bytes($n));
            $post->id = $result;
        } while (checkIdOverlap($post->id, $posts));
        $line = urlencode($post->id) . ';' . urlencode($post->title) . ';' . urlencode($post->text) . PHP_EOL;
    }

    file_put_contents(DATA_FILE, $line, FILE_APPEND);

    return $post->id;
}

function deletePostById(string $id) : void {
    $posts = getAllPosts();
    $line = "";
    foreach ($posts as $existingPost) {
        if ($existingPost->id != $id)
        $line = $line . urlencode($existingPost->id) . ';' . urlencode($existingPost->title) . ';' . urlencode($existingPost->text) . PHP_EOL;
    }
    file_put_contents(DATA_FILE, $line);
}

function getAllPosts() : array {

    $lines = file(DATA_FILE);

    $result = [];
    foreach ($lines as $line) {
        [$id, $title, $text] = explode(';', trim($line));

        $post = new Post(urldecode($title), urldecode($text));
        $post->id = urldecode($id);

        $result[] = $post;
    }

    return $result;
}

function checkIdOverlap(string $id, array $posts) : bool {
    foreach ($posts as $post) {
        if ($post->id == $id) {
            return true;
        }
    }
    return false;
}

function printPosts(array $posts) {
    foreach ($posts as $post) {
        print $post . PHP_EOL;
    }
}