<?php

const USERNAME = 'rasmus_ilmjarv';
const PASSWORD = 'e1cdce';

function getConnection() : PDO {
    $host = 'db.mkalmo.xyz';

    $address = sprintf('mysql:host=%s;port=3306;dbname=%s',
        $host, USERNAME);

    return new PDO($address, USERNAME, PASSWORD);
}
