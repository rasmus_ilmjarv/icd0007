<?php

require_once __DIR__ . '/../connection.php';
require_once __DIR__ . '/MenuItem.php';

foreach (getMenu() as $item) {
    var_dump($item);
    echo "-------------".PHP_EOL;
}

function getMenu() : array {

    $conn = getConnection();

    $stmt = $conn->prepare('SELECT id, parent_id, name FROM menu_item ORDER BY id');

    $menu = [];
    $menuItems = [];

    $stmt->execute();

    foreach ($stmt as $row) {
        $name = $row['name'];
        $id = intval($row['id']);
        $parent_id = intval($row['parent_id']);
        $menuitem = new MenuItem($id, $name);

        if ($parent_id != 0 && key_exists($parent_id, $menuItems)) {
            $menuItems[$parent_id]->addSubItem($menuitem);
        } else if ($parent_id == 0) {
            $menu[] = $menuitem;
        }

        $menuItems[$id] = $menuitem;
    }

    return $menu;
}












function printMenu($items, $level = 0) : void {
    $padding = str_repeat(' ', $level * 3);
    foreach ($items as $item) {
        printf("%s%s\n", $padding, $item->name);
        if ($item->subItems) {
            printMenu($item->subItems, $level + 1);
        }
    }
}
