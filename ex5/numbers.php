<?php

require_once 'connection.php';

$conn = getConnection();
/*
$stmt = $conn->prepare('INSERT INTO number (num) VALUE (:num)');


foreach (range(1, 100) as $item) {
    $num = rand(1, 100);
    $stmt->bindValue(':num', $num);
    $stmt->execute();
}*/

$num = rand(1, 100);

$sql = "" . $num;
/*$result = $conn->query($sql);
echo "Results bigger than ".$num.PHP_EOL;
while($row = $result->fetch()) {
    echo $row['num'].PHP_EOL;
}*/

$stmt = $conn->prepare('SELECT * FROM number WHERE num > :a');
$stmt->bindValue(':a', $num);
$stmt->execute();

foreach ($stmt as $row) {
    print_r($row['num'].PHP_EOL);
}

