<?php

require_once __DIR__ . '/../connection.php';
require_once __DIR__ . '/Contact.php';

print_r(getContacts());

function getContacts() : array {
    $conn = getConnection();

    $contactsByIds = [];

    $stmt = $conn->prepare('SELECT id, name FROM contact');

    $stmt->execute();

    $contacts = [];

    foreach ($stmt as $row) {
        $id = intval($row['id']);
        $name = $row['name'];
        $contact = new Contact($id, $name);
        $contactsByIds[$id] = $contact;
        $contacts[] = $contact;
    }

    $stmt = $conn->prepare('SELECT id, name, number FROM contact, phone WHERE phone.contact_id = contact.id');

    $stmt->execute();



    foreach ($stmt as $row) {
        $id = intval($row['id']);
        $name = $row['name'];
        $number = $row['number'];

        $contact = $contactsByIds[$id];


        $contact->addPhone($number);
    }

    return $contacts;
}