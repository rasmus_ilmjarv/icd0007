<?php

class OrderLine {

    public string $productName;
    public float $price;
    public bool $inStock;

}
