<?php

class OrderLineDao {

    public string $filePath;

    public function __construct($filePath) {
        $this->filePath = $filePath;
    }

    public function getOrderLines() : array {
        $lines = file($this->filePath);

        $orderLines = [];
        foreach ($lines as $line) {

            [$name, $price, $inStock] = explode(';', trim($line));

            $price = floatval($price); // string to float
            $inStock = $inStock === 'true'; // string to boolean

            // create new object and add it to $orderLines list
            $order = new OrderLine();
            $order->productName = $name;
            $order->price = $price;
            $order->inStock = $inStock;
            $orderLines[] = $order;
        }
        return $orderLines;
    }
}
