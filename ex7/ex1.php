<?php

require_once 'OrderLine.php';
require_once 'OrderLineDao.php';

$lines = file('data/order.txt');

$orderLines = new OrderLineDao('data/order.txt');
$orderLines = $orderLines->getOrderLines();

// print list of order line objects
foreach ($orderLines as $orderLine) {
    printf('name: %s, price: %s; in stock: %s' . PHP_EOL,
        $orderLine->productName,
        $orderLine->price,
        $orderLine->inStock ? 'true' : 'false');
}

