<?php

require_once '../vendor/tpl.php';

$errors = ['Pealkiri peab olema 2 kuni 10 märki', 'Hinne peab olema määratud'];
$title = 'Head First HTML and CSS';
$gradeValue = 4;
$isRead = true;
$isEditForm = true;

$data = [
    'errors' => $errors,
    'title' => $title,
    'gradeValue' => $gradeValue,
    'isRead' => $isRead,
    'isEditForm' => $isEditForm
];

print renderTemplate('tpl/form.html', $data);
