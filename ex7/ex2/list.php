<?php

require_once '../vendor/tpl.php';
require_once 'Author.php';
require_once 'Book.php';

$books = [
    new Book('Head First HTML and CSS', 5, true),
    new Book('Learning Web Design', 4, true),
    new Book('Head First Learn to Code', 4, true)
    ];
$books[0]->addAuthor(new Author('Elisabeth', 'Robson'));
$books[0]->addAuthor(new Author('Eric', 'Freeman'));
$books[1]->addAuthor(new Author('Jennifer', 'Robbins'));
$books[2]->addAuthor(new Author('Eric', 'Freeman'));

$data = [
        'books' => $books,
        'subPath' => 'sub_1.html'
];

print renderTemplate('tpl/list.html', $data);
