<?php

require_once 'vendor/tpl.php';
require_once 'Request.php';

$request = new Request($_REQUEST);

//print $request; // display input parameters (for debugging)

$cmd = $request->param('cmd')
    ? $request->param('cmd')
    : 'ctf_form';

if ($cmd === 'ctf_form') {
    $data = [
        'template' => 'ex3_form.html',
        'cmd' => 'ctf_calculate',
    ];

    print renderTemplate('tpl/ex3_main.html', $data);

} else if ($cmd === 'ctf_calculate') {

    $input = $request->param('temperature');

    if (is_numeric($input)) {
        $result = celsiusToFahrenheit($input);
        $data = [
            'template' => 'ex3_result.html',
            'message' => "$input degrees in Celsius is $result degrees in Fahrenheit"
        ];

        print renderTemplate('tpl/ex3_main.html', $data);
    } else {
        $data = [
            'template' => 'ex3_form.html',
            'cmd' => 'ctf_calculate',
            'value' => $input,
            'error' => true,
            'message' => "Input must be a number"
        ];

        print renderTemplate('tpl/ex3_main.html', $data);
    }

} else if ($cmd === 'ftc_calculate') {

    $input = $request->param('temperature');

    if (is_numeric($input)) {
        $result = fahrenheitToCelsius($input);
        $data = [
            'template' => 'ex3_result.html',
            'message' => "$input degrees in Fahrenheit is $result degrees in Celsius"
        ];

        print renderTemplate('tpl/ex3_main.html', $data);
    } else {
        $data = [
            'template' => 'ex3_form.html',
            'cmd' => 'ftc_calculate',
            'value' => $input,
            'error' => true,
            'message' => "Input must be a number"
        ];

        print renderTemplate('tpl/ex3_main.html', $data);
    }

} else if ($cmd === 'ftc_form') {

    $data = [
        'template' => 'ex3_form.html',
        'cmd' => 'ftc_calculate',
    ];

    print renderTemplate('tpl/ex3_main.html', $data);

} else {
    throw new Error('programming error');
}

function celsiusToFahrenheit($temp) : float {
    return round($temp * 9 / 5 + 32, 2);
}

function fahrenheitToCelsius($temp) : float {
    return round(($temp - 32) / (9 / 5), 2);
}

