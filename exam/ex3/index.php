<?php

require_once '../vendor/tpl.php';

if (key_exists('page', $_GET)) {
    $page = intval($_GET["page"]);
} else {
    header('Location: index.php?page=1');
}


if ($page == 2) {
    print renderTemplate("main.html", ['fileName' => 'content2.html', 'content' => 'Page 2']);
} else if ($page == 1) {
    print renderTemplate('main.html', ['fileName' => 'content1.html', 'content' => 'Template 1 Page 1']);
}



