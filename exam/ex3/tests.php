<?php

require_once __DIR__ . '/../vendor/php-test-framework/public-api.php';

const BASE_URL = 'http://localhost:8080/ex3/';

function defaultPageRedirects() {
    disableAutomaticRedirects();

    navigateTo(BASE_URL);

    assertThat(getResponseCode(), isAnyOf(301, 302, 303));
}

function usesFrontControllerPattern() {
    navigateTo(BASE_URL);

    assertFrontControllerLink('page1_link');
    assertFrontControllerLink('page2_link');

}

function canNavigateBetweenPages() {
    navigateTo(BASE_URL);

    clickLinkWithId('page2_link');
    assertThat(getPageText(), containsString('Page 2'));

    clickLinkWithId('page1_link');
    assertThat(getPageText(), containsString('Page 1'));
}

function page1HasCorrectContent() {
    navigateTo(BASE_URL);

    assertThat(getPageText(), containsString('Template 1'));
    assertThat(getPageText(), containsString('Page 1'));
    assertThat(getPageText(), containsString('Link 2'));
}

function page2HasCorrectContent() {
    navigateTo(BASE_URL);

    clickLinkWithId('page2_link');

    assertThat(getPageText(), containsString('Template 2'));
    assertThat(getPageText(), containsString('Page 2'));
    assertThat(getPageText(), containsString('Link 1'));
}

setBaseUrl(BASE_URL);

stf\runTests(new stf\PointsReporter([
    2 => 3,
    3 => 8,
    5 => 20]));

