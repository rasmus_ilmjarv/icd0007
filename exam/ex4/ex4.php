<?php

require_once __DIR__ . '/functions.php';
require_once __DIR__ . '/Student.php';

function getStudentInfo() : array {

    $conn = getConnectionWithData(__DIR__ . '/data.sql');

    $stmt = $conn->prepare('SELECT name, grade FROM student, grade WHERE student.id = grade.id;');

    $stmt->execute();

    $students = [];

    foreach ($stmt as $row) {
        $name = $row['name'];
        if (key_exists($name, $students)) {
            $students[$name][] = intval($row['grade']);
        }
        else {
            $students[$name] = [];
            $students[$name][] = intval($row['grade']);
        }

    }

    $result = [];

    foreach (array_keys($students) as $studentName) {
        $student = new Student($studentName, standardDeviation($students[$studentName]));
        $result[] = $student;
    }

    return $result;
}
