<?php

require "connection.php";
require_once "storage/Book.php";
require_once "storage/Author.php";

function createErrorText($type) {
    switch ($type) {
        case ("title"):
            return "Book title must be between 3 and 23 characters in length.";
        case ("grade"):
            return "You must include a grade for the book.";
        case ("fname"):
            return "Firstname has to be between 1 and 21 characters in length.";
        case ("lname"):
            return "Lastname has to be between 2 and 22 characters in length.";
        case ("authorGrade"):
            return "You must include a grade for the author.";
        case ("author_missing"):
            return "You must choose an author for the book.";
        case ("author_overlap"):
            return "Both authors can't be the same.";
        default:
            return "";
    }
}

function createMSGText($id, $page="books") {
    if ($page == "books") {
        switch ($id) {
            case "delSuccess":
                return "Kustutamine õnnestus!";
            case "addSuccess":
                return "Lisamine õnnestus!";
            case "editSuccess":
                return "Muutmine õnnestus!";
            default:
                return "";
        }
    } elseif ($page == "authors") {
        switch ($id) {
            case "delSuccess":
                return "Kustutamine õnnestus!";
            case "addSuccess":
                return "Lisamine õnnestus!";
            case "editSuccess":
                return "Muutmine õnnestus!";
            default:
                return "";
        }
    }
    return "";
}
