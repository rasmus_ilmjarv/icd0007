<?php

require_once "functions.php";
require_once "ex7/vendor/tpl.php";
require_once "storage/DAO.php";

$dao = new DAO();

if (key_exists("cmd", $_GET)) {
    $cmd = $_GET["cmd"];

    if ($cmd == "book-form") {
        $doError = false;
        $errorText = "";
        $change = false;
        $has_id = false;
        $id = 0;

        $title = "";
        $grade_value = 3;
        $read = false;
        $author_selected = [0, 0];

        if (key_exists("action", $_GET)) {
            $type = $_GET["action"];
            if ($type == "edit") {
                $change = true;
                $id = intval($_GET["id"]);
                $book = $dao->get_book_by_id($id);
            } else {
                if (key_exists("id", $_GET)) {
                    $has_id = true;
                    $id = intval($_GET["id"]);
                }
                $doError = true;
                $errorText = createErrorText($type);
            }
        }
        if (key_exists("title", $_GET)) {
            $title = $_GET["title"];
            $grade_value = intval($_GET["grade"] ?? "");
            $author_selected = [];
            $author_selected[] = intval($_GET["author1"]);
            $author_selected[] = intval($_GET["author2"]);
            $read = $_GET["isRead"] == "on";
        }
        $authors = $dao->read_authors();

        if (key_exists("action", $_GET) && $_GET["action"] == "edit") {

            $data = [
                "page" => "book-form-page",
                "content" => "book-form2.html",
                "doError" => $doError,
                "error" => $errorText,
                "authors" => $authors,
                "change" => $change,
                "book" => $book
            ];
        } else {
            $data = [
                "page" => "book-form-page",
                "content" => "book-form.html",
                "doError" => $doError,
                "title" => $title,
                "error" => $errorText,
                "author_selected" => $author_selected,
                "authors" => $authors,
                "gradeValue" => $grade_value,
                "isRead" => $read,
                "change" => $change,
                "hasId" => $has_id,
                "id" => $id,
            ];
        }

        print renderTemplate("templates/main.html", $data);

    } elseif ($cmd == "book-list") {
        $write_message = key_exists("msg", $_GET);
        $message = "";
        if ($write_message) $message = createMSGText($_GET["msg"]);

        $data = [
            "page" => "book-list-page",
            "content" => "book-table.html",
            "books" => $dao->read_books(),
            "msg" => $message,
            "write_message" => $write_message
        ];

        print renderTemplate("templates/main.html", $data);

    } elseif ($cmd == "author-list") {
        $write_message = key_exists("msg", $_GET);
        $message = "";
        if ($write_message) $message = createMSGText($_GET["msg"], "authors");

        $data = [
            "page" => "author-list-page",
            "content" => "author-table.html",
            "authors" => $dao->read_authors(),
            "msg" => $message,
            "write_message" => $write_message
        ];

        print renderTemplate("templates/main.html", $data);
    } elseif ($cmd == "author-form") {

        $doError = false;
        $errorText = "";
        $change = false;
        $has_id = false;

        $first_name = "";
        $last_name = "";
        $grade_value = 3;

        $id = 0;

        if (key_exists("action", $_GET)) {
            $type = $_GET["action"];
            if ($type == "edit") {
                $change = true;
                $id = intval($_GET["id"]);
                $to_fill = $dao->read_author($id);
                $first_name = $to_fill->firstname;
                $last_name = $to_fill->lastname;
                $grade_value = $to_fill->grade;
            } else {
                if (key_exists("id", $_GET)) {
                    $has_id = true;
                    $id = intval($_GET["id"]);
                }
                $doError = true;
                $errorText = createErrorText($type);
            }
        }

        if (key_exists("firstName", $_GET)) {
            $first_name = $_GET["firstName"];
            $last_name = $_GET["lastName"];
            $grade_value = intval($_GET["grade"] ?? "");
        }

        $data = [
            "page" => "author-form-page",
            "content" => "author-form.html",
            "doError" => $doError,
            "error" => $errorText,
            "change" => $change,
            "has_id" => $has_id,
            "id" => $id,
            "first_name" => $first_name,
            "last_name" => $last_name,
            "gradeValue" => $grade_value
        ];

        print renderTemplate("templates/main.html", $data);
    } else {
        $data = [
            "content" => "404.html"
        ];

        print renderTemplate("templates/main.html", $data);
    }
} else {
    if (key_exists("action", $_GET)) {
        if ($_GET["action"] == "save") {
            if ($_GET["type"] == "book") {
                $dao->validate_book($_POST);
            } elseif ($_GET["type"] == "writer") {
                $dao->validate_author($_POST);
            }
        }
    } else {
        $message = "";
        $data = [
            "page" => "book-list-page",
            "content" => "book-table.html",
            "books" => $dao->read_books(),
            "msg" => $message
        ];

        print renderTemplate("templates/main.html", $data);
    }

}


