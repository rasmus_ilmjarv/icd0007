<?php

class AuthorDTO {
    public string $firstname;
    public string $lastname;
    public ?int $grade;
    public ?int $id;

    public function __construct(string $first, string $last, ?int $grade, ?string $id) {
        $this->firstname = $first;
        $this->lastname = $last;
        $this->grade = intval($grade);
        $this->id = intval($id);
    }

    public function getFullName() {
        return $this->firstname." ".$this->lastname;
    }

    public static function makeAuthor(string $first, string $last, int $grade) {
        $author = new AuthorDTO("", "", 0, "");
        $author->firstname = $first;
        $author->lastname = $last;
        $author->grade = $grade;
        return $author;
    }
}
