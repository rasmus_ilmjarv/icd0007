<?php
require_once "Author.php";

class BookDTO {

    public string $title;
    public ?int $grade;
    public ?bool $isRead;
    public array $authors = [];
    public ?int $id;

    public function __construct(string $title, ?int $grade, ?bool $isRead)
    {
        $this->title = $title;
        $this->grade = $grade;
        $this->isRead = $isRead;
    }


    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function addAuthor($author) {
        if (!is_null($author)) {
            $this->authors[] = $author;
        }

    }

    public function getAuthors() {
        $names = "";
        foreach ($this->authors as $value => $author) {
            if (!is_null($author)) {
                $name = $author->getFullName();
                $names = $names.$name;
                if (!($value == count($this->authors) - 1)) {
                    $names = $names . ", ";
                }
            }

        }
        return $names;
    }

    public function getAuthorIds() {
        $ids = [0, 0];
        if (key_exists(0, $this->authors)){
            if (!(is_null($this->authors[0]))) {
                $ids[0] = $this->authors[0]->id;
            }
        }
        if (key_exists(1, $this->authors)){
            if (!(is_null($this->authors[1]))) {
                $ids[1] = $this->authors[1]->id;
            }
        }
        return $ids;
    }
}
