<?php

require_once "connection.php";
require_once "storage/Book.php";
require_once "storage/Author.php";

class DAO {

    private string $EDIT_MESSAGE = "editSuccess";
    private string $SAVE_MESSAGE = "addSuccess";
    private string $DELETE_MESSAGE = "delSuccess";

    function validate_book(array $post) {

        $authortmp = [];
        $authortmp[] = 1;

        $length = strlen($post["title"]);

        $is_read = key_exists("isRead", $post);

        $data = "title=".urlencode($post["title"])."&"."grade=".urlencode($post["grade"])."&"."isRead=".urlencode($post["isRead"])."&"."author1=".urlencode($post["author1"])."&"."author2=".urlencode($post["author2"]);

        if (key_exists("id", $post)) $data = $data . "&id=". $post["id"];

        if (!(is_int(intval($post["author1"])) && is_int(intval($post["author2"])))) {
            $this->save_book(htmlspecialchars($post['title'], ENT_QUOTES), 1, 3, $is_read);
            header('Location: index.php');
        }

        if (key_exists("deleteButton", $post)) {
            $this->delete_book($post["id"]);
            header("Location: index.php?cmd=book-list&msg=".urlencode($this->DELETE_MESSAGE));

        } elseif (3 > $length || $length > 23) {
            header('Location: index.php?cmd=book-form&action=' . "title" . "&" . $data);

        } elseif (key_exists("id", $post)) {
            $authors = [];
            if ($post["author1"] != 0) $authors[] = $post["author1"];
            if ($post["author2"] != 0) $authors[] = $post["author2"];

            $this->update_book($post['id'], htmlspecialchars($post['title'], ENT_QUOTES), $authors, is_numeric($post['grade']) ? intval($post['grade']) : 0, $is_read);

            header("Location: index.php?cmd=book-list&msg=".urlencode($this->EDIT_MESSAGE));

        } else {
            $authors = [];
            if ($post["author1"] != 0) $authors[] = intval($post["author1"]);
            if ($post["author2"] != 0) $authors[] = intval($post["author2"]);

            $this->save_book(htmlspecialchars($post['title'], ENT_QUOTES), $authors, is_numeric($post['grade']) ? intval($post['grade']) : 0, $is_read);

            header("Location: index.php?cmd=book-list&msg=".urlencode($this->SAVE_MESSAGE));
        }
    }

    function get_book_by_id($id): ?BookDTO
    {
        $connection = getConnection();

        $stmt = $connection->prepare('SELECT b.book_id, title, b.grade, a.author_id, b.is_read FROM books b LEFT JOIN book_author ba ON b.book_id = ba.book_id LEFT JOIN authors a on ba.author_id = a.author_id WHERE  b.book_id = :i');
        $stmt->bindValue(":i", $id);
        $stmt->execute();

        $authors = [];

        $read = [];

        $book = null;

        foreach ($stmt as $entry) {
            $author = $this->read_author($entry['author_id']);
            if (!in_array($entry['book_id'], $read)) {
                $read[] = $entry['book_id'];
                $book = new BookDTO($entry['title'], intval($entry['grade']), $entry['is_read'], null);
                $book->setId(intval($id));
            }
            $book->addAuthor($author);
        }
        return $book;
    }

    function read_books(): array
    {
        $connection = getConnection();

        $stmt = $connection->prepare('SELECT b.book_id, title, b.grade, b.is_read, a.author_id FROM books b LEFT JOIN book_author ba on b.book_id = ba.book_id LEFT JOIN authors a on ba.author_id = a.author_id;');
        $stmt->execute();
        $authors = $this->read_authors();

        $data = [];

        $read = [];

        foreach ($stmt as $entry) {
            $author = $this->find_author($authors, intval($entry['author_id']));
            if (in_array($entry['book_id'], $read)) {
                $book = $data[array_search($entry['book_id'], $read)];
                if ($author) {
                    $book->addAuthor($author);
                }
                continue;
            }
            $book = new BookDTO($entry['title'], intval($entry['grade']), $entry['is_read']);
            $book->addAuthor($author);
            $read[] = $entry['book_id'];
            $book->setId(intval($entry['book_id']));
            $data[] = $book;
        }
        return $data;
    }

    private function find_author(array $authors, int $id) {
        foreach ($authors as $author) {
            if ($author->id == $id) {
                return $author;
            }
        }
        return null;
    }

    function save_book($title, $author_ids, $grade, $is_read) {
        $connection = getConnection();

        $stmt = $connection->prepare('INSERT INTO books (title, grade, is_read) VALUES (:t, :g, :r);');
        $stmt->bindValue(':t', $title);
        $stmt->bindValue(':r', $is_read ? 1 : 0);
        $stmt->bindValue(':g', $grade);
        $stmt->execute();

        $id = $connection->lastInsertId();

        $stmt3 = $connection->prepare('INSERT INTO book_author (book_id, author_id) VALUES (:b, :a);');
        $stmt3->bindValue(':b', $id);
        foreach ($author_ids as $author) {
            $stmt3->bindValue(':a', $author);
            $stmt3->execute();
        }
    }

    function delete_book($id) {
        $connection = getConnection();

        $stmt = $connection->prepare('DELETE FROM books WHERE book_id = :a');
        $stmt->bindValue(':a', $id);
        $stmt->execute();

        $stmt2 = $connection->prepare('DELETE FROM book_author WHERE book_id = :a');
        $stmt2->bindValue(':a', $id);
        $stmt2->execute();
    }

    function update_book($id, $title, $author_ids, $grade, $is_read) {
        $connection = getConnection();

        $stmt = $connection->prepare('UPDATE books SET title = :t, grade = :c, is_read = :r WHERE book_id = :i;');
        $stmt->bindValue(':i', $id);
        $stmt->bindValue(':t', $title);
        $stmt->bindValue(':r', $is_read ? 1 : 0);
        $stmt->bindValue(':c', $grade);
        $stmt->execute();

        $stmt2 = $connection->prepare('DELETE FROM book_author WHERE book_id = :a');
        $stmt2->bindValue(':a', $id);
        $stmt2->execute();

        $stmt3 = $connection->prepare('INSERT INTO book_author (book_id, author_id) VALUES (:b, :a);');
        $stmt3->bindValue(':b', $id);
        foreach ($author_ids as $author) {
            $stmt3->bindValue(':a', $author);
            $stmt3->execute();
        }
    }

    public function validate_author(array $post) {
        $fname_length = strlen($post["firstName"]);
        $lname_length = strlen($post["lastName"]);

        $data = "firstName=".$post["firstName"]."&"."lastName=".$post["lastName"]."&"."grade=".$post["grade"];

        if (key_exists("id", $post)) $data = $data . "&id=". $post["id"];

        if (key_exists("deleteButton", $post)) {
            $this->delete_author(intval($post["id"]));
            header("Location: index.php?cmd=author-list&msg=".urlencode($this->DELETE_MESSAGE));

        } elseif (1 > $fname_length || $fname_length > 21) {
            header("Location:  index.php?cmd=author-form&action=" . "fname&" . $data);

        } elseif (2 > $lname_length || $lname_length > 22) {
            header("Location: index.php?cmd=author-form&action=" . "lname&" . $data);

        }  elseif (!key_exists("grade", $post)) {
            header('Location: index.php?cmd=author-form&action=' . "authorGrade" . "&" . $data);

        } elseif (key_exists("id", $post)) {
            $grade = is_numeric($post['grade']) ? intval($post['grade']) : 0;

            $author = new AuthorDTO($post['firstName'], $post['lastName'], $grade, $post['id']);

            $this->update_author_dto($author);

            header("Location: index.php?cmd=author-list&msg=".$this->EDIT_MESSAGE);

        } else {
            $author = AuthorDTO::makeAuthor($post['firstName'], $post['lastName'], is_numeric($post['grade']) ? intval($post['grade']) : 0);

            $this->save_author_dto($author);

            header("Location: index.php?cmd=author-list&msg=".$this->SAVE_MESSAGE);

        }
    }

    function read_author($id): ?AuthorDTO
    {
        $connection = getConnection();

        $stmt = $connection->prepare('SELECT first_name, last_name, grade FROM authors WHERE author_id = :i');
        $stmt->bindValue(":i", $id);
        $stmt->execute();

        $author = null;

        foreach ($stmt as $entry) {
            $author = new AuthorDTO($entry['first_name'], $entry['last_name'], intval($entry['grade']), $id);
        }
        return $author;
    }

    function read_authors(): array
    {
        $connection = getConnection();

        $stmt = $connection->prepare('SELECT author_id, first_name, last_name, grade FROM authors ORDER BY last_name;');
        $stmt->execute();

        $data = [];

        foreach ($stmt as $entry) {
            $author = new AuthorDTO($entry['first_name'], $entry['last_name'], $entry['grade'], $entry['author_id']);
            $data[] = $author;
        }
        return $data;
    }

    function delete_author($id) {
        $connection = getConnection();

        $stmt = $connection->prepare('DELETE FROM authors WHERE author_id = :a');
        $stmt->bindValue(':a', $id);
        $stmt->execute();

        $stmt2 = $connection->prepare('DELETE FROM book_author WHERE author_id = :a');
        $stmt2->bindValue(':a', $id);
        $stmt2->execute();
    }

    function update_author($id, $first_name, $last_name, $grade) {
        $connection = getConnection();

        $stmt = $connection->prepare('UPDATE authors SET first_name = :a, last_name = :b, grade = :c WHERE author_id = :i');
        $stmt->bindValue(':i', $id);
        $stmt->bindValue(':a', $first_name);
        $stmt->bindValue(':b', $last_name);
        $stmt->bindValue(':c', $grade);
        $stmt->execute();
    }

    function update_author_dto(AuthorDTO $authorDTO) {
        $connection = getConnection();

        $stmt = $connection->prepare('UPDATE authors SET first_name = :a, last_name = :b, grade = :c WHERE author_id = :i');
        $stmt->bindValue(':i', $authorDTO->id);
        $stmt->bindValue(':a', $authorDTO->firstname);
        $stmt->bindValue(':b', $authorDTO->lastname);
        $stmt->bindValue(':c', $authorDTO->grade);
        $stmt->execute();
    }

    function save_author($first_name, $last_name, $grade) {
        $connection = getConnection();

        $stmt = $connection->prepare('INSERT INTO authors (first_name, last_name, grade) VALUES (:f, :l, :g);');
        $stmt->bindValue(':f', $first_name);
        $stmt->bindValue(':l', $last_name);
        $stmt->bindValue(':g', $grade);
        $stmt->execute();
    }

    function save_author_dto(AuthorDTO $authorDTO) {
        $connection = getConnection();

        $stmt = $connection->prepare('INSERT INTO authors (first_name, last_name, grade) VALUES (:f, :l, :g);');
        $stmt->bindValue(':f', $authorDTO->firstname);
        $stmt->bindValue(':l', $authorDTO->lastname);
        $stmt->bindValue(':g', $authorDTO->grade);
        $stmt->execute();
    }
}