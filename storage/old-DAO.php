<?php

require "connection.php";
require_once "storage/Book.php";
require_once "storage/Author.php";

function readBooks(){
    $lines = file("storage/book_storage.txt");
    $data = [];
    foreach ($lines as $line) {
        $line = explode(";", $line);
        $decodedLine = [];
        $decodedLine["title"] = urldecode($line[0]);
        $decodedLine["author"] = urldecode($line[1]);
        $decodedLine["grade"] = urldecode($line[2]);
        $decodedLine["isRead"] = urldecode($line[3]);
        $data[] = $decodedLine;
    }
    return $data;
}


function read_books_DB() {
    $connection = getConnection();

    $stmt = $connection->prepare('SELECT b.book_id, title, b.grade, a.first_name, a.last_name FROM books b, authors a, book_author ba WHERE b.book_id = ba.book_id AND a.author_id = ba.author_id ORDER BY b.book_id;');
    $stmt->execute();

    $data = [];

    $read = [];

    foreach ($stmt as $entry) {
        $author = $entry['first_name'] . " " . $entry['last_name'];
        if (in_array($entry['book_id'], $read)) {
            $book = $data[array_search($entry['book_id'], $read)];
            $book["author"] = $book["author"] . ', ' . $author;
            $data[array_search($entry['book_id'], $read)] = $book;
            continue;
        }
        $book = [];
        $read[] = $entry['book_id'];
        $book['id'] = $entry['book_id'];
        $book['title'] = $entry['title'];
        $book["author"] = $author;
        $book["grade"] = $entry['grade'];
        $data[] = $book;
    }
    return $data;
}

function read_books_Stupid_DB() {
    $connection = getConnection();

    $stmt = $connection->prepare('SELECT b.book_id, ba.book_id, title, b.grade, a.first_name, a.last_name FROM books b LEFT JOIN book_author ba on b.book_id = ba.book_id LEFT JOIN authors a on ba.author_id = a.author_id;');
    $stmt->execute();

    $data = [];

    $read = [];

    foreach ($stmt as $entry) {
        $author = $entry['first_name'] . " " . $entry['last_name'];
        if (in_array($entry['book_id'], $read)) {
            $book = $data[array_search($entry['book_id'], $read)];
            $book["author"] = $book["author"] . ', ' . $author;
            $data[array_search($entry['book_id'], $read)] = $book;
            continue;
        }
        $book = [];
        $read[] = $entry['book_id'];
        $book['id'] = $entry['book_id'];
        $book['title'] = $entry['title'];
        $book["author"] = $author;
        $book["grade"] = $entry['grade'];
        $data[] = $book;
    }
    return $data;
}


function read_books_DTO_Stupid_DB() {
    $connection = getConnection();

    $stmt = $connection->prepare('SELECT b.book_id, title, b.grade, b.is_read, a.author_id FROM books b LEFT JOIN book_author ba on b.book_id = ba.book_id LEFT JOIN authors a on ba.author_id = a.author_id;');
    $stmt->execute();

    $data = [];

    $read = [];

    foreach ($stmt as $entry) {
        $author = read_author_DTO_DB(intval($entry['author_id']));
        if (in_array($entry['book_id'], $read)) {
            $book = $data[array_search($entry['book_id'], $read)];
            if ($author) {
                $book->addAuthor($author);
            }
            error_log("a".count($book->authors));
            continue;
        }
        $book = new BookDTO($entry['title'], intval($entry['grade']), $entry['is_read']);
        $book->addAuthor($author);
        error_log("a".count($book->authors));
        $read[] = $entry['book_id'];
        $book->setId(intval($entry['book_id']));
        $data[] = $book;
    }
    return $data;
}


function read_book_DB($id) {
    $connection = getConnection();

    $stmt = $connection->prepare('SELECT b.book_id, title, b.grade, a.author_id, b.is_read FROM books b LEFT JOIN book_author ba ON b.book_id = ba.book_id LEFT JOIN authors a on ba.author_id = a.author_id WHERE  b.book_id = :i');
    $stmt->bindValue(":i", $id);
    $stmt->execute();

    $authors = [];

    $read = [];

    $book = [];

    foreach ($stmt as $entry) {
        $authors[] = $entry['author_id'];
        print "aaaaaa";
        if (!in_array($entry['book_id'], $read)) {
            error_log("AAAAAAAAAAAAAAAAAAAAA");
            $read[] = $entry['book_id'];
            $book['id'] = $entry['book_id'];
            $book['title'] = $entry['title'];
            $book["grade"] = $entry['grade'];
            $book['isRead'] = $entry['is_read'];
        }
    }
    $book["author"] = $authors;
    return $book;
}

function read_book_DTO_DB($id): ?BookDTO
{
    $connection = getConnection();

    $stmt = $connection->prepare('SELECT b.book_id, title, b.grade, a.author_id, b.is_read FROM books b LEFT JOIN book_author ba ON b.book_id = ba.book_id LEFT JOIN authors a on ba.author_id = a.author_id WHERE  b.book_id = :i');
    $stmt->bindValue(":i", $id);
    $stmt->execute();

    $authors = [];

    $read = [];

    $book = null;

    foreach ($stmt as $entry) {
        $author = read_author_DTO_DB($entry['author_id']);
        if (!in_array($entry['book_id'], $read)) {
            $read[] = $entry['book_id'];
            $book = new BookDTO($entry['title'], intval($entry['grade']), $entry['is_read'], null);
            $book->setId(intval($id));
        }
        $book->addAuthor($author);
    }
    return $book;
}

function save_book_DB($title, $author_ids, $grade, $is_read) {
    $connection = getConnection();

    $stmt = $connection->prepare('INSERT INTO books (title, grade, is_read) VALUES (:t, :g, :r);');
    $stmt->bindValue(':t', $title);
    $stmt->bindValue(':r', $is_read ? 1 : 0);
    $stmt->bindValue(':g', $grade);
    $stmt->execute();

    $id = $connection->lastInsertId();/*
    $stmt2 = $connection->prepare('SELECT book_id FROM books WHERE title = :t AND grade = :g AND is_read = :r;');
    $stmt2->bindValue(':t', $title);
    $stmt2->bindValue(':r', $is_read ? 1 : 0);
    $stmt2->bindValue(':g', $grade);
    $stmt2->execute();
    foreach ($stmt2 as $entry) {
        $id = $entry['book_id'];
    }*/
    error_log($id);

    $stmt3 = $connection->prepare('INSERT INTO book_author (book_id, author_id) VALUES (:b, :a);');
    $stmt3->bindValue(':b', $id);
    foreach ($author_ids as $author) {
        $stmt3->bindValue(':a', $author);
        $stmt3->execute();
    }
}

function readAuthors(){
    $lines = file("storage/author_storage.txt");
    $data = [];
    foreach ($lines as $line) {
        $line = explode(";", $line);
        $decodedLine = [];
        $decodedLine["firstName"] = urldecode($line[0]);
        $decodedLine["lastName"] = urldecode($line[1]);
        $decodedLine["grade"] = urldecode($line[2]);
        $data[] = $decodedLine;
    }
    return $data;
}

function read_authors_DB() {
    $connection = getConnection();

    $stmt = $connection->prepare('SELECT author_id, first_name, last_name, grade FROM authors ORDER BY last_name;');
    $stmt->execute();

    $data = [];

    foreach ($stmt as $entry) {
        $author = [];
        $author['id'] = $entry['author_id'];
        $author["firstName"] = $entry['first_name'];
        $author["lastName"] = $entry['last_name'];
        $author["grade"] = $entry['grade'];
        $data[] = $author;
    }
    return $data;
}

function read_authors_DTO_DB(): array
{
    $connection = getConnection();

    $stmt = $connection->prepare('SELECT author_id, first_name, last_name, grade FROM authors ORDER BY last_name;');
    $stmt->execute();

    $data = [];

    foreach ($stmt as $entry) {
        $author = new AuthorDTO($entry['first_name'], $entry['last_name'], $entry['grade'], $entry['author_id']);
        $data[] = $author;
    }
    return $data;
}

function read_author_DB($id) {
    $connection = getConnection();

    $stmt = $connection->prepare('SELECT first_name, last_name, grade FROM authors WHERE author_id = :i');
    $stmt->bindValue(":i", $id);
    $stmt->execute();

    $author = [];

    foreach ($stmt as $entry) {
        $author = [];
        $author["firstName"] = $entry['first_name'];
        $author["lastName"] = $entry['last_name'];
        $author["grade"] = $entry['grade'];
    }

    return $author;
}

function read_author_DTO_DB($id): ?AuthorDTO
{
    $connection = getConnection();

    $stmt = $connection->prepare('SELECT first_name, last_name, grade FROM authors WHERE author_id = :i');
    $stmt->bindValue(":i", $id);
    $stmt->execute();

    $author = null;

    foreach ($stmt as $entry) {
        $author = new AuthorDTO($entry['first_name'], $entry['last_name'], intval($entry['grade']), $id);
    }
    return $author;
}

function save_author_DB($first_name, $last_name, $grade) {
    $connection = getConnection();

    $stmt = $connection->prepare('INSERT INTO authors (first_name, last_name, grade) VALUES (:f, :l, :g);');
    $stmt->bindValue(':f', $first_name);
    $stmt->bindValue(':l', $last_name);
    $stmt->bindValue(':g', $grade);
    $stmt->execute();
}

function deleteBook($id) {
    $data = readBooks();
    $line = "";
    for ($i = 0; $i < count($data); $i++) {

        if ($i == intval($id)) {
            error_log("done");
            continue;
        }error_log($i);
        $line = $line . urlencode($data[$i]["title"]).";".urlencode($data[$i]["author"]).";".$data[$i]["grade"].";".$data[$i]["isRead"];
    }
    error_log($line);
    file_put_contents("storage/book_storage.txt", $line);
}

function deleteAuthor($id) {
    $data = readAuthors();
    $line = "";
    for ($i = 0; $i < count($data); $i++) {

        if ($i == intval($id)) {
            error_log("done");
            continue;
        }
        error_log($i);
        $line = $line . urlencode($data[$i]["firstName"]) . ";" . urlencode($data[$i]["lastName"]) . ";" . $data[$i]["grade"];
    }
    error_log($line);
    file_put_contents("storage/author_storage.txt", $line);
}

function delete_author_DB($id) {
    $connection = getConnection();
    error_log($id);

    $stmt = $connection->prepare('DELETE FROM authors WHERE author_id = :a');
    $stmt->bindValue(':a', $id);
    $stmt->execute();

    $stmt2 = $connection->prepare('DELETE FROM book_author WHERE author_id = :a');
    $stmt2->bindValue(':a', $id);
    $stmt2->execute();
}

function delete_book_DB($id) {
    $connection = getConnection();

    $stmt = $connection->prepare('DELETE FROM books WHERE book_id = :a');
    $stmt->bindValue(':a', $id);
    $stmt->execute();

    $stmt2 = $connection->prepare('DELETE FROM book_author WHERE book_id = :a');
    $stmt2->bindValue(':a', $id);
    $stmt2->execute();
}

function update_author_DB($id, $first_name, $last_name, $grade) {
    $connection = getConnection();

    $stmt = $connection->prepare('UPDATE authors SET first_name = :a, last_name = :b, grade = :c WHERE author_id = :i');
    $stmt->bindValue(':i', $id);
    $stmt->bindValue(':a', $first_name);
    $stmt->bindValue(':b', $last_name);
    error_log($grade);
    $stmt->bindValue(':c', $grade);
    $stmt->execute();
}

function update_book_DB($id, $title, $author_ids, $grade, $is_read) {
    $connection = getConnection();

    $stmt = $connection->prepare('UPDATE books SET title = :t, grade = :c, is_read = :r WHERE book_id = :i;');
    $stmt->bindValue(':i', $id);
    $stmt->bindValue(':t', $title);
    $stmt->bindValue(':r', $is_read ? 1 : 0);
    error_log("0-0");
    $stmt->bindValue(':c', $grade);
    $stmt->execute();

    $stmt2 = $connection->prepare('DELETE FROM book_author WHERE book_id = :a');
    $stmt2->bindValue(':a', $id);
    $stmt2->execute();

    $stmt3 = $connection->prepare('INSERT INTO book_author (book_id, author_id) VALUES (:b, :a);');
    $stmt3->bindValue(':b', $id);
    foreach ($author_ids as $author) {
        $stmt3->bindValue(':a', $author);
        $stmt3->execute();
    }
}